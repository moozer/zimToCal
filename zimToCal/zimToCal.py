#!/usr/bin/env python3


from .support import setup_arg_parse, task_to_cal

if __name__ == "__main__":
    config = setup_arg_parse()
    print (task_to_cal(config))
