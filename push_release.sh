
#!/bin/bash

# see https://gitlab.com/gitlab-org/gitlab-ce/issues/31221#note_140029643
# and https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html

set -e

cat <<EOF > release.json
{
	"name": "$CI_COMMIT_TAG",
	"tag_name": "$CI_COMMIT_TAG",
	"description": "$CI_COMMIT_TITLE",
	"assets":
	{
		"links":
		[
			{ 	"name": "zimToCal-$CI_COMMIT_TAG.tar.gz",
				  "url": "$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_TAG/raw/dist/zimToCal-$CI_COMMIT_TAG.tar.gz?job=build_package"
			},
			{ 	"name": "zimToCal-${CI_COMMIT_TAG}-py2.py3-none-any.whl",
				  "url": "$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_TAG/raw/dist/zimToCal-${CI_COMMIT_TAG}-py2.py3-none-any.whl?job=build_package"
			}
		]
	}
}

EOF

curl -v --header 'Content-Type: application/json'  \
     --header "PRIVATE-TOKEN: $RELEASEAPIKEY" \
     --data '@release.json' \
     --request POST $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases | tee curl.log

FIRST_WORD=$(cat curl.log | cut -d ':' -f1)
echo "First word of curl return value is $FIRST_WORD"
if [ "x$FIRST_WORD" == 'x{"message"' ]; then
  echo "return value starts with message, means failure...."
  exit 1
fi
